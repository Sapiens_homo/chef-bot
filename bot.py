import discord
import discord.ext
import json
import os
DBG=False;
intents=discord.Intents.all();
intents.members=True;
client=discord.Client(intents=intents);
chefcount=dict();
count_path="/app/data/";
TOKEN=os.environ.get("TOKEN");
if DBG:
    count_path="./";
keywords=dict();
@client.event
async def on_ready():
    global chefcount;
    global keywords;
    data=None;
    data2=None;
    with open(count_path+"chefcount.json",'r') as f:
        data=f.read();
    with open(count_path+"keyword.json",'r') as f:
        data2=f.read();
    chefcount=json.loads(data);
    keywords=json.loads(data2);
    if DBG:
        print(keywords.keys());
    print("Login as:",client.user);
    return;
@client.event
async def on_message(message:discord.Message):
    if message.author==client.user:
        return;
    if message.author.bot:
        return;
    global chefcount;
    global keywords;
    guild=str(message.guild.id);
    if message.content.startswith("%chefrank") or message.content.startswith('%rank'):
        if(guild not in chefcount.keys()):
            await message.channel.send("No one have been chefed. It's better to chef someone now.");
            return;
        # sort chefcount[message.guild]
        j=0;
        emb=discord.Embed(title='Chef Rank' ,description='See dian shens has been chefed for how many times');
        sorted_chefcount=dict(sorted(chefcount[guild].items(), key=lambda x:x[1], reverse= True));
        for i in sorted_chefcount.keys():
            j+=1;
            t=' time'
            if sorted_chefcount[i]-1:
                t=' times'
            emb.add_field(name='**#'+str(j)+'**',value='<@!'+str(i)+'>: '+str(sorted_chefcount[i])+t,inline=True);
            if j==9:
                break;
        await message.channel.send(embed=emb)
        return;
    if message.content.startswith("%chef"):
        try:
            chefee=str(( message.mentions[0] ).id);
        except:
            chefee=str(message.author.id);
        time=' times';
        if guild not in chefcount.keys():
            chefcount[guild]=dict();
        if chefee in chefcount[guild].keys():
            chefcount[guild][chefee]+=1;
        else:
            chefcount[ guild ][ chefee ]=1;
            time=' time';
        with open(count_path+"chefcount.json",'w') as f:
            json.dump(chefcount,f);
        await message.channel.send("<@!"+str(chefee)+"> so dian, has been chefed "+str(chefcount[guild][chefee])+time);
        return;
    msg=message.content.lower().split(" ");
    head=msg[0];
    end=msg[-1];
    sent=0;
    #print(head, end);
    for i in keywords["head"]:
        if head==i:
            await message.channel.send(yes_or_no(), reference=message, mention_author=False);
            sent=1;
            break;
        continue;
    if not sent:
        for j in keywords["tail"]:
            if end==j:
                await message.channel.send(yes_or_no(), reference=message, mention_author=False);
                break;
            continue;
    if message.content.startswith( "choose from" ):
        msg=( ( message.content.split( " ", 2 ) )[2] ).split( "\"" );
        while( " " in msg ):
            msg.pop( msg.index( " " ) );
        while( "" in msg ):
            msg.pop( msg.index( "" ) );
        await message.channel.send( "I choose " + msg[ get_random() % len( msg ) ] );
    if "機率" in message.content.lower() or "probability" in msg or "ratio" in msg or "rate" in msg:
        await message.channel.send(str((get_random()%10001)/100.0)+"%", reference=message, mention_author=False);
        return;
    return;
def get_random():
    r = list( os.getrandom( 8, 0 ) );
    ret = 1;
    for i in range( len( r ) ):
        ret += r[ i ] * ( 2 ** ( 8 * ( len( r ) - i -1 ) ) );
    return ret;
def yes_or_no():
    a = get_random() % 101;
    r="";
    if a>50:
        r="Yes.";
    elif a<50:
        r="No.";
    else:
        r="I don\'t know.";
    return r;
client.run(TOKEN);
