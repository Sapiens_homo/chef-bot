Chef Bot
==========
A Discord Bot counts a person who has been chefed for how many times to show how **DIAN(電)** ze is.
----------

## **THIS PROJECT IS NO LONGER MAINTAINED due to the unreasoned-banishment of my Discord Account**

### Getting Started
I use [Discord.py](https://discordpy.readthedocs.io/en/stable/) to make this bot, tested with Python 3.10 and Discord.py v2.0.5 on Arch Linux and RHEL8

Line 4 in bot.py
```py
TOKEN='token'
```
You should change it to your token by change it directly, environment variables, or other files.
I use **JSON** to store data. It can store with SQL instead, or no offline storage.

##### Install dependencies(Normal mode)
```sh
sudo pip install -r requirements.txt
```
or
```sh
pip install --user -r requirements.txt
```

##### Build Docker Image
```sh
docker image -t "your image name" .
```

### Run bot
- Normal
	```sh
	export TOKEN="Your Bot Token"
	python bot.py
	```
	or
	```sh
	./run.sh
	```
- Docker (not tested)
	```sh
	docker volume create <VOLUME>
	docker service create -d \
	--replicas=4 \
	--name <NAME> \
	--mount source=<VOLUME>,target=/app/data \
	<IMAGE>
	```
