from python:3
workdir /app
copy . .
env TOKEN ""
run pip install --upgrade pip
run pip install -r ./requirements.txt
run mkdir data
run mv chefcount.json data/chefcount.json
run mv keyword.json data/keyword.json
cmd ["python","./bot.py"]
